package com.amico.amico.injection.component;

import com.amico.amico.injection.PerActivity;
import com.amico.amico.injection.module.ActivityModule;
import com.amico.amico.ui.auth.register.RegisterActivity;
import com.amico.amico.ui.auth.signIn.SignInActivity;
import com.amico.amico.ui.intro.SplashActivity;
import com.amico.amico.ui.main.MainActivity;
import com.amico.amico.ui.main.report1.Report1Activity;
import com.amico.amico.ui.main.report2.Report2Activity;
import com.amico.amico.ui.main.report3.Report3Activity;
import com.amico.amico.ui.main.report4.Report4Activity;
import com.amico.amico.ui.main.report5.Report5Activity;

import dagger.Subcomponent;

/**
 * This component inject dependencies to all Activities across the application
 */
@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(SplashActivity splashActivity);
    void inject(SignInActivity signInActivity);
    void inject(RegisterActivity registerActivity);
    void inject(MainActivity mainActivity);
    void inject(Report1Activity report1Activity);
    void inject(Report2Activity report2Activity);
    void inject(Report3Activity report3Activity);
    void inject(Report4Activity report4Activity);
    void inject(Report5Activity report5Activity);
}
