package com.amico.amico.injection.component;

import android.app.Application;
import android.content.Context;

import com.amico.amico.data.DataManager;
import com.amico.amico.data.SyncService;
import com.amico.amico.data.local.DatabaseHelper;
import com.amico.amico.data.local.PreferencesHelper;
import com.amico.amico.data.remote.RibotsService;
import com.amico.amico.injection.ApplicationContext;
import com.amico.amico.injection.module.ApplicationModule;
import com.amico.amico.util.RxEventBus;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(SyncService syncService);

    @ApplicationContext
    Context context();
    Application application();
    RibotsService ribotsService();
    PreferencesHelper preferencesHelper();
    DatabaseHelper databaseHelper();
    DataManager dataManager();
    RxEventBus eventBus();

}
