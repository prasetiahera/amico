package com.amico.amico.injection.component;

import com.amico.amico.injection.PerFragment;
import com.amico.amico.injection.module.FragmentModule;

import dagger.Subcomponent;

@PerFragment
@Subcomponent(modules = FragmentModule.class)
public interface FragmentComponent extends ActivityComponent {

}