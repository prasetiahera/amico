package com.amico.amico.data.model;

public class UserProfile {
    private String id;
    private String full_name;
    private String name;
    private String gender;
    private String birth_date;
    private String email;
    private String date_created;

    public UserProfile(){

    }

    public UserProfile(String id, String full_name, String name, String gender, String birth_date, String email, String date_created) {
        this.id = id;
        this.full_name = full_name;
        this.name = name;
        this.gender = gender;
        this.birth_date = birth_date;
        this.email = email;
        this.date_created = date_created;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }
}
