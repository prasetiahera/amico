package com.amico.amico.ui.auth.register;

import android.content.Context;

import com.amico.amico.ui.base.MvpView;

public interface RegisterMvpView extends MvpView {
    Context getContext();

    void showAlertDialog(String message);

    void showProgressDialog(String message);

    void hideProgressDialog();

    void showToastMessage(String message);

    void selectManGender();

    void selectWomanGender();

    void showDatePickerDialog();

    void selectBirthDate();

    void toMainActivity();

}
