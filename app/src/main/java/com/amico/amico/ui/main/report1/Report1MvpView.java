package com.amico.amico.ui.main.report1;

import com.amico.amico.ui.base.MvpView;

public interface Report1MvpView extends MvpView {
    void toHome();

    void toReport2();
}
