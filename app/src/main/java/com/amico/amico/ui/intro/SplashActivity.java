package com.amico.amico.ui.intro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.amico.amico.R;
import com.amico.amico.ui.auth.signIn.SignInActivity;
import com.amico.amico.ui.base.BaseActivity;

import javax.inject.Inject;

public class SplashActivity extends BaseActivity implements SplashMvpView {

    public static int SPLASH_TIME_OUT = 2500;

    @Inject
    SplashPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_splash);
        presenter.attachView(this);
        runSplash();
    }

    @Override
    public void runSplash() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent signIn = new Intent(SplashActivity.this, SignInActivity.class);
                startActivity(signIn);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
