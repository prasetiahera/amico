package com.amico.amico.ui.main.report1;

import com.amico.amico.data.DataManager;
import com.amico.amico.ui.base.BasePresenter;

import javax.inject.Inject;

public class Report1Presenter extends BasePresenter<Report1MvpView> {
    private DataManager dataManager;

    @Inject
    public Report1Presenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(Report1MvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }
}
