package com.amico.amico.ui.main.report5;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.widget.EditText;

import com.amico.amico.R;
import com.amico.amico.ui.base.BaseActivity;
import com.amico.amico.ui.main.MainActivity;
import com.ramotion.fluidslider.FluidSlider;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Report5Activity extends BaseActivity implements Report5MvpView{

    @Inject Report5Presenter presenter;

    @BindView(R.id.et_report5_answer) EditText etReport5Answer;
    @BindView(R.id.ib_report5_option_1) AppCompatImageButton ibReport5Option1;
    @BindView(R.id.ib_report5_option_2) AppCompatImageButton ibReport5Option2;
    @BindView(R.id.ib_report5_option_3) AppCompatImageButton ibReport5Option3;
    @BindView(R.id.ib_report5_option_4) AppCompatImageButton ibReport5Option4;
    @BindView(R.id.bt_report5_send) AppCompatButton btReport5Send;
    @BindView(R.id.bt_home) AppCompatImageButton btHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_report5);
        ButterKnife.bind(this);
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @OnClick(R.id.bt_home)
    @Override
    public void toHome() {
        Intent home = new Intent (this, MainActivity.class);
        startActivity(home);
    }

    @OnClick(R.id.ib_report5_option_1)
    @Override
    public void selectOption1() {
        ibReport5Option1.setEnabled(false);
        ibReport5Option1.setImageResource(R.drawable.check);
        ibReport5Option2.setEnabled(true);
        ibReport5Option2.setImageResource(0);
        ibReport5Option3.setEnabled(true);
        ibReport5Option3.setImageResource(0);
        ibReport5Option4.setEnabled(true);
        ibReport5Option4.setImageResource(0);
    }

    @OnClick(R.id.ib_report5_option_2)
    @Override
    public void selectOption2() {
        ibReport5Option2.setEnabled(false);
        ibReport5Option2.setImageResource(R.drawable.check);
        ibReport5Option1.setEnabled(true);
        ibReport5Option1.setImageResource(0);
        ibReport5Option3.setEnabled(true);
        ibReport5Option3.setImageResource(0);
        ibReport5Option4.setEnabled(true);
        ibReport5Option4.setImageResource(0);
    }

    @OnClick(R.id.ib_report5_option_3)
    @Override
    public void selectOption3() {
        ibReport5Option3.setEnabled(false);
        ibReport5Option3.setImageResource(R.drawable.check);
        ibReport5Option1.setEnabled(true);
        ibReport5Option1.setImageResource(0);
        ibReport5Option2.setEnabled(true);
        ibReport5Option2.setImageResource(0);
        ibReport5Option4.setEnabled(true);
        ibReport5Option4.setImageResource(0);
    }

    @OnClick(R.id.ib_report5_option_4)
    @Override
    public void selectOption4() {
        ibReport5Option4.setEnabled(false);
        ibReport5Option4.setImageResource(R.drawable.check);
        ibReport5Option1.setEnabled(true);
        ibReport5Option1.setImageResource(0);
        ibReport5Option2.setEnabled(true);
        ibReport5Option2.setImageResource(0);
        ibReport5Option3.setEnabled(true);
        ibReport5Option3.setImageResource(0);
    }
}
