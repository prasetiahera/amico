package com.amico.amico.ui.auth.register;

import android.support.annotation.NonNull;
import android.util.Log;

import com.amico.amico.R;
import com.amico.amico.data.DataManager;
import com.amico.amico.data.model.UserProfile;
import com.amico.amico.ui.base.BasePresenter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import static com.amico.amico.util.FirebaseUtils.getFirebaseAuth;
import static com.amico.amico.util.FirebaseUtils.getFirebaseFirestore;
import static com.amico.amico.util.FirebaseUtils.getFirebaseUser;

public class RegisterPresenter extends BasePresenter<RegisterMvpView> {
    private DataManager dataManager;
    private static final String TAG = "RegisterActivity";

    @Inject
    public RegisterPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(RegisterMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public void initActivity() {
        if(isViewAttached()){
            getMvpView().selectManGender();
            getMvpView().selectBirthDate();
        }
    }

    private boolean checkBlankFields(String fullName, String name, String birthDate, String email, String password, String confirmPassword){
        if(fullName.length() == 0 || name.length() == 0 || birthDate.length() ==0 || email.length() == 0 || password.length() == 0 || confirmPassword.length() == 0)
            if(isViewAttached()){
                getMvpView().showAlertDialog(
                        getMvpView().getContext().getString(R.string.alert_fields_blank));
                return false;
            }
        return true;
    }

    private boolean checkEmailFormat(String email){
        if(!isValidEmailAddress(email))
            if(isViewAttached()){
                getMvpView().showAlertDialog(
                        getMvpView().getContext().getString(R.string.alert_email_format_wrong));
                return false;
        }
        return true;
    }

    private boolean checkWeakPassword(String password){
        if(password.length() < 8)
            if(isViewAttached()){
                getMvpView().showAlertDialog(
                        getMvpView().getContext().getString(R.string.alert_password_weak));
                return false;
        }
        return true;
    }

    private boolean checkPasswordEquality(String password, String confirmPassword){
        if(!password.equals(confirmPassword))
            if(isViewAttached()){
            getMvpView().showAlertDialog(getMvpView().getContext().getString(R.string.alert_password_different));
            return false;
        }
        return true;
    }

    private boolean isValidEmailAddress (String email){
        if(isViewAttached()){
            String ePattern = getMvpView().getContext().getString(R.string.regex_email);
            java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
            java.util.regex.Matcher m = p.matcher(email);
            return m.matches();
        }
        return false;
    }

    public void verifyUserRegisterCredential(String fullName, String name, String gender, String birthDate, String email, String password, String confirmPassword){
        if (!checkBlankFields(fullName, name, birthDate, email, password, confirmPassword)) return;
        if (!checkEmailFormat(email)) return;
        if (!checkWeakPassword(password)) return;
        if (!checkPasswordEquality(password, confirmPassword)) return;

        registerUser(fullName, name, gender, birthDate, email, password);
    }

    private void registerUser(final String fullName, final String name, final String gender, final String birthDate, String email, String password){
        if(isViewAttached())
            getMvpView().showProgressDialog(getMvpView().getContext().getString(R.string.progress_registering_user));
        getFirebaseAuth().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            Log.d(TAG, "Register User : Success" );
                            storeUserDataToDatabase(getFirebaseUser(), fullName, name, gender, birthDate);
                        } else {
                            Log.w(TAG, "Register User : Failed", task.getException());
                            if (isViewAttached()) getMvpView().hideProgressDialog();
                            if (isViewAttached()) getMvpView().showAlertDialog(getMvpView().getContext().getString(R.string.alert_register_failed));
                        }
                    }
                });
    }

    private void storeUserDataToDatabase(FirebaseUser user, String fullName, String name, String gender, String birthDate){
        UserProfile userProfile = new UserProfile(user.getUid(), fullName, name, gender, birthDate, user.getEmail(), getDateCreated());

        getFirebaseFirestore().collection("Users").document(user.getUid())
                .set(userProfile)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "Store registered user database : Success");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Store registered user database : Failed", e);
                    }
                });

        if (isViewAttached()) getMvpView().hideProgressDialog();
        if (isViewAttached()) getMvpView().toMainActivity();
        if (isViewAttached()) getMvpView().showToastMessage(getMvpView().getContext().getString(R.string.alert_register_success));
    }

    private String getDateCreated() {
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(date);

        return formattedDate;
    }

}
