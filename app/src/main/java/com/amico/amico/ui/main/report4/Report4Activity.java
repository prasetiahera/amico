package com.amico.amico.ui.main.report4;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.widget.EditText;

import com.amico.amico.R;
import com.amico.amico.ui.base.BaseActivity;
import com.amico.amico.ui.main.MainActivity;
import com.amico.amico.ui.main.report5.Report5Activity;
import com.ramotion.fluidslider.FluidSlider;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Report4Activity extends BaseActivity implements Report4MvpView {

    @Inject Report4Presenter presenter;

    @BindView(R.id.et_report4_answer) EditText etReport4Answer;
    @BindView(R.id.slider4) FluidSlider slider4;
    @BindView(R.id.bt_report4_next) AppCompatButton btReport4Next;
    @BindView(R.id.bt_home) AppCompatImageButton btHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_report4);
        ButterKnife.bind(this);
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @OnClick(R.id.bt_report4_next)
    @Override
    public void toReport5() {
        Intent report5 = new Intent (this, Report5Activity.class);
        startActivity(report5);
    }

    @OnClick(R.id.bt_home)
    @Override
    public void toHome() {
        Intent home = new Intent (this, MainActivity.class);
        startActivity(home);
    }
}
