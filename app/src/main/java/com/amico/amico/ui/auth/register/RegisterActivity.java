package com.amico.amico.ui.auth.register;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amico.amico.R;
import com.amico.amico.ui.auth.signIn.SignInActivity;
import com.amico.amico.ui.base.BaseActivity;
import com.amico.amico.ui.main.MainActivity;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity implements RegisterMvpView {

    private static final String TAG = "Register Activity";

    @Inject
    RegisterPresenter presenter;

    @BindView(R.id.et_full_name) EditText etFullName;
    @BindView(R.id.et_name) EditText etName;
    @BindView(R.id.ll_man) LinearLayout llMan;
    @BindView(R.id.ll_woman) LinearLayout llWoman;
    @BindView(R.id.tv_man) TextView tvMan;
    @BindView(R.id.tv_woman) TextView tvWoman;
    @BindView(R.id.tv_birth_date) TextView tvBirthDate;
    @BindView(R.id.et_email) EditText etEmail;
    @BindView(R.id.et_password) EditText etPassword;
    @BindView(R.id.et_confirm_password) EditText etConfirmPassword;
    @BindView(R.id.bt_register) AppCompatButton btRegister;

    private DatePickerDialog.OnDateSetListener dateSetListener;
    private ProgressDialog progressDialog;
    private String gender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        presenter.attachView(this);
        presenter.initActivity();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showAlertDialog(String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this)
                .setMessage(message)
                .setNeutralButton(R.string.dialog_action_ok, null);
        Dialog dialog = alertDialog.create();
        dialog.show();
    }

    @Override
    public void showProgressDialog(String message) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @OnClick (R.id.ll_man)
    @Override
    public void selectManGender() {
        llMan.setEnabled(false);
        llMan.setBackgroundColor(getResources().getColor(R.color.primary_light_green));
        tvMan.setTextColor(getResources().getColor(R.color.white));
        llWoman.setEnabled(true);
        llWoman.setBackground(getResources().getDrawable(R.drawable.sign_in_text_field));
        tvWoman.setTextColor(getResources().getColor(R.color.primary_green));
        gender = getString(R.string.text_gender_man);
    }

    @OnClick (R.id.ll_woman)
    @Override
    public void selectWomanGender() {
        llWoman.setEnabled(false);
        llWoman.setBackgroundColor(getResources().getColor(R.color.primary_light_green));
        tvWoman.setTextColor(getResources().getColor(R.color.white));
        llMan.setEnabled(true);
        llMan.setBackground(getResources().getDrawable(R.drawable.sign_in_text_field));
        tvMan.setTextColor(getResources().getColor(R.color.primary_green));
        gender = getString(R.string.text_gender_woman);
    }

    @OnClick (R.id.tv_birth_date)
    @Override
    public void showDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(
                RegisterActivity.this, R.style.Theme_AppCompat_Light_Dialog_MinWidth,
                dateSetListener,
                year, month, day);
        dialog.getWindow();
        dialog.show();
    }

    @Override
    public void selectBirthDate() {
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                Log.d(TAG, "onDataSet: dd/mm/yyyy:" + dayOfMonth + "/" + month + "/" + year);
                String date = dayOfMonth + "/" + month + "/" + year;
                tvBirthDate.setText(date);
            }
        };
    }

    @OnClick(R.id.bt_register)
    public void verifyUserCredential() {
        presenter.verifyUserRegisterCredential(
                etFullName.getText().toString().trim(),
                etName.getText().toString().trim(),
                gender,
                tvBirthDate.getText().toString().trim(),
                etEmail.getText().toString().trim(),
                etPassword.getText().toString().trim(),
                etConfirmPassword.getText().toString().trim()
        );
    }

    @Override
    public void toMainActivity() {
        Intent main = new Intent(this, MainActivity.class);
        startActivity(main);
        finish();
    }
}
