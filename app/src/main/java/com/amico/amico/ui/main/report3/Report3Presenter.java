package com.amico.amico.ui.main.report3;

import com.amico.amico.data.DataManager;
import com.amico.amico.ui.base.BasePresenter;

import javax.inject.Inject;

public class Report3Presenter extends BasePresenter<Report3MvpView> {
    private DataManager dataManager;

    @Inject
    public Report3Presenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(Report3MvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }
}
