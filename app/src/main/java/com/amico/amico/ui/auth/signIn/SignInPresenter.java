package com.amico.amico.ui.auth.signIn;

import android.support.annotation.NonNull;
import android.util.Log;

import com.amico.amico.R;
import com.amico.amico.data.DataManager;
import com.amico.amico.ui.base.BasePresenter;
import com.amico.amico.util.SharedPrefManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.firestore.DocumentSnapshot;

import javax.inject.Inject;

import static com.amico.amico.util.FirebaseUtils.getFirebaseAuth;
import static com.amico.amico.util.FirebaseUtils.getFirebaseFirestore;
import static com.amico.amico.util.FirebaseUtils.getFirebaseUser;

public class SignInPresenter extends BasePresenter<SignInMvpView> {
    private DataManager dataManager;
    private static final String TAG = "Sign In Activity";

    @Inject
    public SignInPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(SignInMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public void initActivity(){
        if(isViewAttached() && checkLogin()){
            getMvpView().toMainActivity();
        }
    }

    private boolean checkLogin(){
        return getFirebaseAuth().getCurrentUser() != null;
    }

    private boolean checkBlankFields(String email, String password){
        if (email.length() == 0 || password.length() == 0){
            if (isViewAttached()) getMvpView().showAlertDialog(getMvpView().getContext().getString(R.string.alert_fields_blank));
            return false;
        };
        return true;
    }

    public void verifyUserLoginCredential(String email, String password){
        if (!checkBlankFields(email, password)) return;

        signInUser(email, password);
    }

    private void signInUser(String email, String password){
        if (isViewAttached()) getMvpView().showProgressDialog(getMvpView().getContext().getString(R.string.progress_sign_in_user));
        getFirebaseAuth().signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "Sign In User : Success");
                            getFirebaseUser();
                            if(isViewAttached()){
                                getMvpView().hideProgressDialog();
                                getMvpView().toMainActivity();
                            }
                        }
                        else {
                            Log.w(TAG, "Sign User : Failed");
                            if (isViewAttached()){
                                getMvpView().hideProgressDialog();
                                getMvpView().showAlertDialog(getMvpView().getContext().getString(R.string.progress_sign_in_failed));
                            }
                        }
                    }
                });
    }
}
