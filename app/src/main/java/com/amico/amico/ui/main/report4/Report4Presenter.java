package com.amico.amico.ui.main.report4;

import com.amico.amico.data.DataManager;
import com.amico.amico.ui.base.BasePresenter;

import javax.inject.Inject;

public class Report4Presenter extends BasePresenter<Report4MvpView> {
    private DataManager dataManager;

    @Inject
    public Report4Presenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(Report4MvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }
}
