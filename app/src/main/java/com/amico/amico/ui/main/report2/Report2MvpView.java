package com.amico.amico.ui.main.report2;

import com.amico.amico.ui.base.MvpView;

public interface Report2MvpView extends MvpView {
    void toHome();

    void toReport3();
}
