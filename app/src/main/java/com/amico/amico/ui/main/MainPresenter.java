package com.amico.amico.ui.main;

import android.support.annotation.NonNull;
import android.util.Log;

import com.amico.amico.data.DataManager;
import com.amico.amico.ui.base.BasePresenter;
import com.amico.amico.util.SharedPrefManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestoreSettings;

import javax.inject.Inject;

import static com.amico.amico.util.FirebaseUtils.getFirebaseAuth;
import static com.amico.amico.util.FirebaseUtils.getFirebaseFirestore;
import static com.amico.amico.util.FirebaseUtils.getFirebaseUser;

public class MainPresenter extends BasePresenter<MainMvpView> {
    private DataManager dataManager;
    private static final String TAG = "Main Activity";

    @Inject
    public MainPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(MainMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public void initActivity(){
        getUserName();
    }

    private void getUserName(){
        if(getFirebaseUser() != null) {
            getFirebaseFirestore().collection("Users").document(getFirebaseUser().getUid())
                    .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot documentSnapshot = task.getResult();
                        String fullName = documentSnapshot.getString("full_name");
                        String name = documentSnapshot.getString("name");
                        String gender = documentSnapshot.getString("gender");
                        String birth_date = documentSnapshot.getString("birth_date");
                        String email = documentSnapshot.getString("email");

                        if (isViewAttached()) getMvpView().setUserName(name);
                        Log.d(TAG, "Get User Name: " + name);
                    } else {
                        Log.w(TAG, "Cannot Retrieve User Name", task.getException());
                    }
                }
            });
        }

    }

    public void signOut(){
        getFirebaseAuth().signOut();
        if (isViewAttached()) getMvpView().toSignInActivity();
    }
}
