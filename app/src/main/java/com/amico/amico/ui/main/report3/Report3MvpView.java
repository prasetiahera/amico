package com.amico.amico.ui.main.report3;

import com.amico.amico.ui.base.MvpView;

public interface Report3MvpView extends MvpView {
    void toHome();

    void toReport4();
}
