package com.amico.amico.ui.auth.signIn;

import android.content.Context;

import com.amico.amico.ui.base.MvpView;

public interface SignInMvpView extends MvpView {
    Context getContext();

    void showAlertDialog(String message);

    void showProgressDialog(String message);

    void hideProgressDialog();

    void toRegisterActivity();

    void toMainActivity();
}
