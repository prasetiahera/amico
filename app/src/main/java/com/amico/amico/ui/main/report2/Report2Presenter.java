package com.amico.amico.ui.main.report2;

import com.amico.amico.data.DataManager;
import com.amico.amico.ui.base.BasePresenter;

import javax.inject.Inject;

public class Report2Presenter extends BasePresenter<Report2MvpView> {
    private DataManager dataManager;

    @Inject
    public Report2Presenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(Report2MvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }
}
