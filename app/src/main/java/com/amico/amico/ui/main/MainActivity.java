package com.amico.amico.ui.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageButton;
import android.widget.TextView;

import com.amico.amico.R;
import com.amico.amico.ui.auth.signIn.SignInActivity;
import com.amico.amico.ui.base.BaseActivity;
import com.amico.amico.ui.main.report1.Report1Activity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements MainMvpView {

    @Inject MainPresenter presenter;

    @BindView(R.id.tv_main_title_name) TextView tvMainTitleName;
    @BindView(R.id.ib_amico) AppCompatImageButton ibAmico;
    @BindView(R.id.ib_menu_1) AppCompatImageButton ibMenu1;
    @BindView(R.id.ib_menu_2) AppCompatImageButton ibMenu2;
    @BindView(R.id.ib_menu_3) AppCompatImageButton ibMenu3;
    @BindView(R.id.ib_menu_4) AppCompatImageButton ibMenu4;
    @BindView(R.id.ib_menu_5) AppCompatImageButton ibMenu5;
    @BindView(R.id.ib_user_info) AppCompatImageButton ibUserInfo;
    @BindView(R.id.ib_sign_out) AppCompatImageButton ibSignOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter.attachView(this);
        presenter.initActivity();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setUserName(String name){
        tvMainTitleName.setText(name);
    }

    @OnClick (R.id.ib_amico)
    @Override
    public void toReport1() {
        Intent report1 = new Intent(this, Report1Activity.class);
        startActivity(report1);
    }

    @OnClick(R.id.ib_sign_out)
    public void signOutUser(){
        presenter.signOut();
    }

    @Override
    public void toSignInActivity() {
        Intent signIn = new Intent(this, SignInActivity.class);
        startActivity(signIn);
        finish();
    }
}
