package com.amico.amico.ui.main.report5;

import com.amico.amico.ui.base.MvpView;

public interface Report5MvpView extends MvpView {
    void toHome();

    void selectOption1();

    void selectOption2();

    void selectOption3();

    void selectOption4();
}
