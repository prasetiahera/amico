package com.amico.amico.ui.main.report5;

import com.amico.amico.data.DataManager;
import com.amico.amico.ui.base.BasePresenter;

import javax.inject.Inject;

public class Report5Presenter extends BasePresenter<Report5MvpView> {
    private DataManager dataManager;

    @Inject
    public Report5Presenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(Report5MvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }
}
