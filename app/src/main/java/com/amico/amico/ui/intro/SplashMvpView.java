package com.amico.amico.ui.intro;

import com.amico.amico.ui.base.MvpView;

public interface SplashMvpView extends MvpView {
    void runSplash();
}
