package com.amico.amico.ui.main;

import android.content.Context;

import com.amico.amico.ui.base.MvpView;

public interface MainMvpView extends MvpView {
    Context getContext();

    void setUserName(String name);

    void toReport1();

    void toSignInActivity();
}
