package com.amico.amico.ui.main.report2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.widget.EditText;

import com.amico.amico.R;
import com.amico.amico.ui.base.BaseActivity;
import com.amico.amico.ui.main.MainActivity;
import com.amico.amico.ui.main.report3.Report3Activity;
import com.ramotion.fluidslider.FluidSlider;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Report2Activity extends BaseActivity implements Report2MvpView{

    @Inject Report2Presenter presenter;

    @BindView(R.id.et_report2_answer) EditText etReport2Answer;
    @BindView(R.id.slider2) FluidSlider slider2;
    @BindView(R.id.bt_report2_next) AppCompatButton btReport2Next;
    @BindView(R.id.bt_home) AppCompatImageButton btHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_report2);
        ButterKnife.bind(this);
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @OnClick(R.id.bt_report2_next)
    @Override
    public void toReport3() {
        Intent report3 = new Intent (this, Report3Activity.class);
        startActivity(report3);
    }

    @OnClick(R.id.bt_home)
    @Override
    public void toHome() {
        Intent home = new Intent (this, MainActivity.class);
        startActivity(home);
    }
}
