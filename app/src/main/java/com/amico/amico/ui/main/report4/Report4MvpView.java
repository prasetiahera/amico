package com.amico.amico.ui.main.report4;

import com.amico.amico.ui.base.MvpView;

public interface Report4MvpView extends MvpView {
    void toHome();

    void toReport5();
}
