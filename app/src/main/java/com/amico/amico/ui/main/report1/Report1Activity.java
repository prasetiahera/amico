package com.amico.amico.ui.main.report1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.widget.EditText;

import com.amico.amico.R;
import com.amico.amico.ui.base.BaseActivity;
import com.amico.amico.ui.main.MainActivity;
import com.amico.amico.ui.main.report2.Report2Activity;
import com.ramotion.fluidslider.FluidSlider;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Report1Activity extends BaseActivity implements Report1MvpView {

    @Inject Report1Presenter presenter;

    @BindView(R.id.et_report1_answer) EditText etReport1Answer;
    @BindView(R.id.slider1) FluidSlider slider1;
    @BindView(R.id.bt_report1_next) AppCompatButton btReport1Next;
    @BindView(R.id.bt_home) AppCompatImageButton btHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_report1);
        ButterKnife.bind(this);
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @OnClick(R.id.bt_report1_next)
    @Override
    public void toReport2() {
        Intent report2 = new Intent (this, Report2Activity.class);
        startActivity(report2);
    }

    @OnClick(R.id.bt_home)
    @Override
    public void toHome() {
        Intent home = new Intent (this, MainActivity.class);
        startActivity(home);
    }
}
