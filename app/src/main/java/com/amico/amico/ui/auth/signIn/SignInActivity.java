package com.amico.amico.ui.auth.signIn;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.widget.EditText;

import com.amico.amico.R;
import com.amico.amico.ui.auth.register.RegisterActivity;
import com.amico.amico.ui.base.BaseActivity;
import com.amico.amico.ui.main.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInActivity extends BaseActivity implements SignInMvpView {

    @Inject
    SignInPresenter presenter;

    @BindView(R.id.et_email) EditText etEmail;
    @BindView(R.id.et_password) EditText etPassword;
    @BindView(R.id.bt_sign_in) AppCompatButton btSignIn;
    @BindView(R.id.bt_to_register) AppCompatButton btRegister;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        presenter.attachView(this);
        presenter.initActivity();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showAlertDialog(String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this)
                .setMessage(message)
                .setNeutralButton(R.string.dialog_action_ok, null);
        Dialog dialog = alertDialog.create();
        dialog.show();
    }

    @Override
    public void showProgressDialog(String message) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.dismiss();
    }

    @OnClick (R.id.bt_sign_in)
    public void verifyUserCredential(){
        presenter.verifyUserLoginCredential(
                etEmail.getText().toString().trim(),
                etPassword.getText().toString().trim()
        );
    }

    @Override
    public void toMainActivity() {
        Intent main = new Intent (this, MainActivity.class);
        startActivity(main);
        finish();
    }

    @OnClick (R.id.bt_to_register)
    @Override
    public void toRegisterActivity() {
        Intent register = new Intent (this, RegisterActivity.class);
        startActivity(register);
    }
}
