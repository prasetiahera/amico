package com.amico.amico.ui.main.report3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.widget.EditText;

import com.amico.amico.R;
import com.amico.amico.ui.base.BaseActivity;
import com.amico.amico.ui.main.MainActivity;
import com.amico.amico.ui.main.report4.Report4Activity;
import com.ramotion.fluidslider.FluidSlider;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Report3Activity extends BaseActivity implements Report3MvpView {

    @Inject Report3Presenter presenter;

    @BindView(R.id.et_report3_answer) EditText etReport3Answer;
    @BindView(R.id.slider3) FluidSlider slider3;
    @BindView(R.id.bt_report3_next) AppCompatButton btReport3Next;
    @BindView(R.id.bt_home) AppCompatImageButton btHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_report3);
        ButterKnife.bind(this);
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @OnClick(R.id.bt_report3_next)
    @Override
    public void toReport4() {
        Intent report4 = new Intent (this, Report4Activity.class);
        startActivity(report4);
    }

    @OnClick(R.id.bt_home)
    @Override
    public void toHome() {
        Intent home = new Intent (this, MainActivity.class);
        startActivity(home);
    }
}
