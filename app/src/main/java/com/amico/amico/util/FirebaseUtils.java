package com.amico.amico.util;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class FirebaseUtils {

    private static FirebaseAuth firebaseAuth;
    private static FirebaseDatabase firebaseDatabase;
    private static FirebaseStorage firebaseStorage;
    private static FirebaseFirestore firebaseFirestore;
    private static final String NODE_USER = "users";
    private static final String NODE_USER_AVATAR = "userAvatars";

    public static FirebaseAuth getFirebaseAuth() {
        if (firebaseAuth == null) {
            firebaseAuth = FirebaseAuth.getInstance();
        }
        return firebaseAuth;
    }

    public static boolean checkIsLogin() {
        if (getFirebaseAuth().getCurrentUser() != null) {
            return true;
        }
        return false;
    }

    public static FirebaseDatabase getFirebaseDatabase() {
        if (firebaseDatabase == null) {
            firebaseDatabase = FirebaseDatabase.getInstance();
            firebaseDatabase.setPersistenceEnabled(true);
        }
        return firebaseDatabase;
    }

    public static FirebaseUser getFirebaseUser() {
        return getFirebaseAuth().getCurrentUser();
    }

    public static DatabaseReference getUserRef() {
        return getFirebaseDatabase().getReference().child(NODE_USER);
    }

    public static DatabaseReference getUserRef(String userId) {
        return getFirebaseDatabase().getReference()
                .child(NODE_USER)
                .child(userId);
    }

    public static FirebaseStorage getFirebaseStorage() {
        if (firebaseStorage == null) {
            firebaseStorage = FirebaseStorage.getInstance();
        }
        return firebaseStorage;
    }

    public static StorageReference getUserProfileStorageReference(String timestamp) {
        return getFirebaseStorage().getReference()
                .child(NODE_USER)
                .child(NODE_USER_AVATAR)
                .child(timestamp);
    }

    public static FirebaseFirestore getFirebaseFirestore() {
        return FirebaseFirestore.getInstance();
    }

    public static CollectionReference getCollectionRef(){
        return FirebaseFirestore.getInstance().collection("Users");
    }
}
